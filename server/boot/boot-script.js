'use strict';
module.exports = function(app) {
  var mysqlDs = app.dataSources.testdb;
  var Categories = app.models.Category;
  var Equipe = app.models.Equipe;
  var Personne = app.models.Personne;
  var Pool = app.models.Pool;

  // first autoupdate the `Category` model to avoid foreign key constraint failure
  mysqlDs.autoupdate('Category', function(err) {
    if (err) throw err;
    console.log('\nAutoupdated table `Categories`.');

    mysqlDs.autoupdate('Equipe', function(err) {
      if (err) throw err;
      console.log('\nAutoupdated table `Equipes`.');
    })
    mysqlDs.autoupdate('Personne', function(err) {
      if (err) throw err;
      console.log('\nAutoupdated table `Peronnes`.');
    });
    mysqlDs.autoupdate('Pool', function(err) {
      if (err) throw err;
      console.log('\nAutoupdated table `Pools`.');
    });
    mysqlDs.autoupdate('Tournois', function(err) {
      if (err) throw err;
      console.log('\nAutoupdated table `Pools`.');
    });
    mysqlDs.autoupdate('Games', function(err) {
      if (err) throw err;
      console.log('\nAutoupdated table `Pools`.');
    });
  });
};
