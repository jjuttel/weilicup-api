#Install Loopback

npm install -g loopback-cli

#TO create a MYSQL DB in DOCKER run: source setup.sh <HOST> <PORT> <USER> <PASSWORD> <DATABASE>
go to cd node_modules/loopback-connector-mysql/

source setup.sh 127.0.0.1 3306 root pass testdb

Download MYSQL workbench and create a new schema
CREATE SCHEMA `testdb` ;

# Run loopback with
go to root dir /weilicup-api

node .

check runnning

Browse your REST API at http://0.0.0.0:3000/explorer

Web server listening at: http://0.0.0.0:3000/


#create new model

lb model

to enable automigration add the following scipt to boot-script.js
    
    var TABLEVAR = app.models.TABLENAME;
     
    mysqlDs.autoupdate('TABLEVAR', function(err) {
      if (err) throw err;
      console.log('\nAutoupdated table `TABLEVAR`.');
    });

#create new relation

lb relation


